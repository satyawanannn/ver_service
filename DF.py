from deepface import DeepFace
import numpy as np 
import cv2
import sklearn.metrics as skm

model = DeepFace.build_model('Facenet')

def encode_im(str_byt):
	img_np = cv2.imdecode(str_byt, cv2.IMREAD_COLOR)

	enc = np.array(DeepFace.represent(img_np, model_name='Facenet', model=model))
	
	return enc

def verify(test_face, photos):
	distance = 0
	for photo in photos:
		face = photo.face
		distance += skm.pairwise_distances([test_face], [face], metric='cosine')

	distance /= len(photos)

	if distance<0.4:
		return 'Verified'
	else:
		return 'Not Verified'