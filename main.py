import numpy as np
import DF
import werkzeug
from flask import Flask, request, Response
from flask_restful import Api, Resource, reqparse, abort, fields, marshal_with
from flask_sqlalchemy import SQLAlchemy
from PIL import Image
import io

app = Flask(__name__)
api = Api(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
db = SQLAlchemy(app)


class PhotosModel(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100), nullable=False)
	face = db.Column(db.PickleType, nullable=False)
	photo = db.Column(db.LargeBinary, nullable=False)
	# def __repr__(self):
	# 	return "ID = {}, Name = {}".format(id, name)

#run once
db.create_all()

class GetPhotos(Resource):
	@marshal_with({'id':fields.Integer, 'name':fields.String})
	def get(self, name):
		result = PhotosModel.query.filter_by(name=name).all()
		if not result:
			abort(404, message="Could not find photo...")

		return result
api.add_resource(GetPhotos, "/get_photo/<string:name>")

class ShowPhoto(Resource):
	def get(self, row_id):
		result = PhotosModel.query.filter_by(id=row_id).first()

		image = Image.open(io.BytesIO(result.photo))
		image.show()
		# if not result:
		# 	abort(404, message="Could not find photo...")

		# return Response(result.photo, mimetype='jpg')
api.add_resource(ShowPhoto, "/show_photo/<int:row_id>")


add_photos_args = reqparse.RequestParser()
add_photos_args.add_argument("file", type=werkzeug.datastructures.FileStorage, location="files") 
class AddPhoto(Resource):
	# @marshal_with(resource_fields)
	def put(self, name):
		args = add_photos_args.parse_args()
		file = args['file'].read()
		
		t = np.frombuffer(file, dtype=np.uint8)
		# img_np = cv2.imdecode(t, cv2.IMREAD_COLOR)

		enc = DF.encode_im(t)

		new_photo = PhotosModel(name=name, face=enc, photo=file)
		db.session.add(new_photo)
		db.session.commit()

		return 'Photo of {} added successfully!'.format(name)
		# return name
api.add_resource(AddPhoto, "/add_photo/<string:name>")

update_args = reqparse.RequestParser()
update_args.add_argument('name',type=str)
update_args.add_argument("file", type=werkzeug.datastructures.FileStorage, location="files")
class UpdatePhoto(Resource):
	def put(self, row_id):
		result = PhotosModel.query.filter_by(id=row_id).first()
		if not result:
			abort(404, message="Could not find photo...")

		args = update_args.parse_args()
		name, file = '',''

		if (args['name']):
			result.name = args['name']
		
		if (args['file']):
			file = args['file'].read()
			t = np.frombuffer(file, dtype=np.uint8)
			enc = DF.encode_im(t)

			result.face = enc
			result.photo = file

		db.session.commit()

		return 'Photo with ID {} updated...'.format(row_id) 
api.add_resource(UpdatePhoto,'/update/<int:row_id>')

delete_photo_args = reqparse.RequestParser()
delete_photo_args.add_argument('id', type=str)
class DeletePhoto(Resource):
	def delete(self, id):
		args = delete_photo_args.parse_args()
		# image_id = args['id']
		image_id = id

		result = PhotosModel.query.filter_by(id=image_id).all()
		if not result:
			abort(404, message="Could not find photo with ID {}".format(image_id))
		else:
			PhotosModel.query.filter_by(id=image_id).delete()
			db.session.commit()

			return 'Photo with ID {} deleted'.format(image_id)
api.add_resource(DeletePhoto, "/delete_photo/<int:id>")

similarity_args = reqparse.RequestParser()
similarity_args.add_argument("file", type=werkzeug.datastructures.FileStorage, location="files")
class Verify(Resource):
	# @marshal_with(resource_fields)
	def get(self, name):
		args = similarity_args.parse_args()
		file = args['file'].read()
		
		t = np.frombuffer(file, dtype=np.uint8)

		enc = DF.encode_im(t)

		result = PhotosModel.query.filter_by(name=name).all()

		str_verify = DF.verify(enc, result)

		return str_verify
api.add_resource(Verify, "/verify/<string:name>")

if __name__ == "__main__":
	app.run(debug=True)