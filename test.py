import requests
import os
import re
import argparse

BASE = "http://127.0.0.1:5000/"

def get_args():
    parser = argparse.ArgumentParser(description="Add, Delete, Verify Images")
    parser.add_argument('-d','--dir', type=str, help="Input images folder directory. Images file name must be in this format (person_name.jpg)")
    parser.add_argument('-pd','--photo_dir', type=str, help="Input image file directory")
    parser.add_argument('-n','--name', type=str, help="Input person name")
    parser.add_argument('-id','--row_id', type=str, help="Input row id")
    
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-ais', '--add_images', action='store_true', help="Add new image from a folder. required param : -d")
    group.add_argument('-ai', '--add_image', action='store_true', help="Add new image from a picture file. required param : -pd, -n")
    group.add_argument('-ui', '--update_image', action='store_true', help="Update row. required param : -id. Input -n and -pd if you want to change the name and photo")
    group.add_argument('-di', '--delete_image', action='store_true', help="Delete row. required param : -id")
    group.add_argument('-vi', '--verify_image', action='store_true', help="Verify an image. required param : -pd -n")
    group.add_argument('-gpi', '--get_person_image', action='store_true', help="Get image by name. required param : -n")
    group.add_argument('-sp', '--show_photo', action='store_true', help="Show Image. required param : -id")

    return parser.parse_args()

# # ADD MULTIPLE IMAGE
def add_images(dir):

	files = os.listdir(dir)

	for f in files:
		if(f.endswith('.jpg')):

			files = {'file':open(dir+f, 'rb')}
			photo_name = re.findall('([a-zA-Z]+)\d+.jpg',f)[0]
			response = requests.put(BASE + "add_photo/{}".format(photo_name), files=files)
			print(response.json())

#ADD SINGLE IMAGE
def add_image(dir, name):
	# dir = "D:/Work/Face Recognition/Photos/tes/adit1.jpg"
	# name = "adit"

	files = {'file':open(dir,'rb')}
	response = requests.put(BASE+"add_photo/"+name, files=files)
	print(response.json())

#UPDATE
def update(dir, name, id):
	if dir is None:
		files = None
	else:
		files = {'file':open(dir,'rb')}

	if name is None:
		data = None
	else:
		data = {'name': name}
	
	response = requests.put(BASE+'update/'+id,data=data,files=files)
	print(response.json())

#DELETE AN IMAGE
def delete(id):
	response = requests.delete(BASE+"delete_photo/"+id)
	print(response.json())

#VERIFY IMAGE
def verify(dir, name):
	# files = {'file':open('D:/Work/Face Recognition/Photos/val/iis9.jpg', 'rb')}
	files = {'file':open(dir, 'rb')}
	response = requests.get(BASE + "verify/"+name, files=files)
	print(response.json())

#GET PERSON'S IMAGE
def get_person_im(name):
	response = requests.get(BASE+"get_photo/"+name)
	# response = requests.get(BASE+"get_photo/dita")
	print(response.json())

#SHOW PHOTO by ID
#RUN ON BROWSER
def show_photo(id):
	response = requests.get(BASE+"show_photo/"+id)

if __name__=="__main__":
    args = get_args()
    
    if args.add_images:
    	add_images(args.dir)
    elif args.add_image:
    	add_image(args.photo_dir, args.name)
    elif args.update_image:
    	update(args.photo_dir, args.name, args.row_id)
    elif args.delete_image:
    	delete(args.row_id)
    elif args.verify_image:
    	verify(args.photo_dir, args.name)
    elif args.get_person_image:
    	get_person_im(args.name)
    elif args.show_photo:
    	show_photo(args.row_id)